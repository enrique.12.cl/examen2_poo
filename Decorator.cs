using System;

namespace Examen2_POO 
{
public class Decorador : HeladoBase
{
    protected HeladoBase orden;
    public Decorador (HeladoBase orden)
    {
        this.orden = orden;
        
    }

    public override double CalculoTotalPrecio()
    {
        return orden.CalculoTotalPrecio();
    }

}
}