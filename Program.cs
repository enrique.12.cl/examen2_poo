﻿using System;

namespace Examen2_POO
{
    class Program
    {
        public static void Main(string[] arg)
        {

            var IceCream = new HeladoRomPasas();
            Console.WriteLine("$"+IceCream.CalculoTotalPrecio());

            var adereso1 = new Topping1(IceCream);
            Console.WriteLine("$"+adereso1.CalculoTotalPrecio());

            var adereso2 = new Topping2(adereso1);
            Console.WriteLine("$"+adereso2.CalculoTotalPrecio());

            Console.WriteLine();
        }
    }
}
