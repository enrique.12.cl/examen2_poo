using System;
namespace Examen2_POO
{
    public class Topping3 : Decorador
    {
        public Topping3 (HeladoBase orden) : base (orden)
        {

        }
        public override double CalculoTotalPrecio()
        {
            var Adiccion = base.CalculoTotalPrecio() + 0.35;
            return Adiccion;
        }
    }
}


